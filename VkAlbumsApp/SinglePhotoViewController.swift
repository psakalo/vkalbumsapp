//
//  SinglePhotoViewController.swift
//  VkAlbumsApp
//
//  Created by Pavel Sakalo on 1/29/17.
//  Copyright © 2017 Pavel Sakalo. All rights reserved.
//

import UIKit

class SinglePhotoViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var imageView: UIImageView!
    
    var photoUrl: URL? {
        didSet {
            if isViewLoaded {
                reloadPhoto()
            }
        }
    }
    
    private func reloadPhoto() {
        scrollView.isHidden = true
        activityIndicatorView.startAnimating()
        if photoUrl != nil {
            URLSession.shared.dataTask(with: photoUrl!) { [weak self] data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async { [weak self] in
                    self?.imageView.image = UIImage(data: data)
                    self?.scrollView.isHidden = false
                    self?.activityIndicatorView.stopAnimating()
                }
                }.resume()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        reloadPhoto()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

}
