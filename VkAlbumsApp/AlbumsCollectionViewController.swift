//
//  AlbumsTableViewController.swift
//  VkAlbumsApp
//
//  Created by Pavel Sakalo on 1/26/17.
//  Copyright © 2017 Pavel Sakalo. All rights reserved.
//

import UIKit
import VK_ios_sdk

class AlbumsTableViewController: UICollectionViewController {
    
    static private let systemAlbumIds = ["-6": "profile", "-7": "wall", "-15": "saved"]
    
    var sdk: VKSdk? = nil
    private var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: Model
    
    private var albums: Array<Dictionary<String, Any>>? = nil {
        didSet {
            self.collectionView?.reloadData()
            if let count = albums?.count, count > 0 {
                activityIndicatorView.stopAnimating()
            }
        }
    }
    
    func updateAlbums() {
        if let albumsRequest = VKApi.photos().prepareRequest(withMethodName: "getAlbums", parameters: ["need_system": "1", "need_covers": "1"]) {
            albumsRequest.execute(resultBlock: { [weak self] response in
                if let items = (response?.json as? Dictionary<String, Any>)?["items"] as? Array<Dictionary<String, Any>> {
                    self?.albums = items
                }
            }, errorBlock: { error in
                print("updateAlbums error \(error.debugDescription)")
            })
        }
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Albums"
        
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        collectionView?.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
        
        sdk = VKSdk.initialize(withAppId: VK_APP_ID)
        sdk?.register(self)
        
        VKSdk.wakeUpSession(VK_PERMISSIONS_SCOPE) { state, error in
            DispatchQueue.main.async { [weak self] in
                switch (state) {
                case .authorized:
                    self?.updateAlbums()
                default:
                    self?.showLoginView()
                }
            }
        }
        
        collectionView?.register(UINib(nibName: "PhotoCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        
        cell.imageUrl = URL(string: albums?[indexPath.item]["thumb_src"] as! String)
        cell.title = albums?[indexPath.item]["title"] as? String
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showSingleAlbum", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSingleAlbum", let destinationVc = segue.destination as? PhotosCollectionViewController {
            if let selectedItemIndex = collectionView?.indexPathsForSelectedItems?.first?.item {
                let albumId = String(albums?[selectedItemIndex]["id"] as! Int)
                let realAlbumId = AlbumsTableViewController.systemAlbumIds[albumId] ?? albumId
                destinationVc.albumId = realAlbumId
                destinationVc.albumTitle = albums?[selectedItemIndex]["title"] as? String
            }
        }
    }
    
    // MARK: Interface Builder
    
    @IBAction private func logOut() {
        VKSdk.forceLogout()
        showLoginView()
        albums = nil
    }
    
}

// MARK: VKSdkDelegate implementation
extension AlbumsTableViewController: VKSdkDelegate {
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        DispatchQueue.main.async { [weak self] in
            if result.token != nil {
                self?.updateAlbums()
            }
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        // Not processed
    }
    
}


// MARK: Utils
extension AlbumsTableViewController {
    
    func showLoginView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let loginVc = storyboard.instantiateViewController(withIdentifier: "loginView") as? LoginViewController {
            loginVc.sdk = sdk
            self.present(loginVc, animated: true, completion: nil)
        }
    }
    
}
