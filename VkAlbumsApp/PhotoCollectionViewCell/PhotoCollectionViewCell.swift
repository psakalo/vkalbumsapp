//
//  PhotosCollectionViewCell.swift
//  VkAlbumsApp
//
//  Created by Pavel Sakalo on 1/28/17.
//  Copyright © 2017 Pavel Sakalo. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "Cell"
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var imageNameLabel: UILabel!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    var imageUrl: URL? {
        didSet {
            reloadImage()
        }
    }
    
    var title: String? {
        didSet {
            imageNameLabel.text = title
        }
    }
    
    private func reloadImage() {
        imageView.isHidden = true
        activityIndicatorView.startAnimating()
        if imageUrl != nil {
            URLSession.shared.dataTask(with: imageUrl!) { [weak self] data, response, error in
                guard let data = data, error == nil else { return }
                let image = UIImage(data: data)
                DispatchQueue.main.async { [weak self] in
                    self?.imageView.image = image
                    self?.imageView.isHidden = false
                    self?.activityIndicatorView.stopAnimating()
                }
            }.resume()
        }
    }
    
}
