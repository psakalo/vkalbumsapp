//
//  ViewController.swift
//  VkAlbumsApp
//
//  Created by Pavel Sakalo on 1/26/17.
//  Copyright © 2017 Pavel Sakalo. All rights reserved.
//

import UIKit
import VK_ios_sdk

class LoginViewController: UIViewController {
    
    var sdk: VKSdk? = nil
    
    @IBOutlet private weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sdk?.uiDelegate = self
        sdk?.register(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction private func loginPressed() {
        VKSdk.authorize(VK_PERMISSIONS_SCOPE)
    }

}

// MARK: VKSdkDelegate implementation
extension LoginViewController: VKSdkDelegate {
    
    private func showErrorAlert(withMessage message: String!) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        DispatchQueue.main.async { [weak self] in
            if result.token != nil {
                self?.presentingViewController?.dismiss(animated: true, completion: nil)
            } else {
                self?.showErrorAlert(withMessage: "Some error occured during login process. Please try again.")
            }
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        DispatchQueue.main.async { [weak self] in
            self?.showErrorAlert(withMessage: "Some error occured during login process. Please try again.")
        }
    }
    
}

// MARK: VKSdkUIDelegate implementation
extension LoginViewController: VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        DispatchQueue.main.async { [weak self] in
            self?.present(controller, animated: true, completion: nil)
        }
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print("vkSdkNeedCaptchaEnter")
    }
    
}

