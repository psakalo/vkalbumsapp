//
//  PhotosCollectionViewController.swift
//  VkAlbumsApp
//
//  Created by Pavel Sakalo on 1/28/17.
//  Copyright © 2017 Pavel Sakalo. All rights reserved.
//

import UIKit
import VK_ios_sdk

class PhotosCollectionViewController: UICollectionViewController {
    
    private var activityIndicatorView: UIActivityIndicatorView!
    
    var albumId: String? {
        didSet {
            updatePhotos()
        }
    }
    
    var albumTitle: String? {
        didSet {
            title = albumTitle
        }
    }
    
    // MARK: Model
    
    private var photos: Array<Dictionary<String, Any>>? = nil {
        didSet {
            self.collectionView?.reloadData()
            if let count = photos?.count, count > 0 {
                activityIndicatorView.stopAnimating()
            }
        }
    }
    
    private func updatePhotos() {
        if let albumsRequest = VKApi.photos().prepareRequest(withMethodName: "get", parameters: ["album_id": albumId ?? "", "photo_sizes": "1"]) {
            albumsRequest.execute(resultBlock: { [weak self] response in
                if let items = (response?.json as? Dictionary<String, Any>)?["items"] as? Array<Dictionary<String, Any>> {
                    self?.photos = items
                }
                }, errorBlock: { error in
                    print("updatePhotos error \(error.debugDescription)")
            })
        }
    }
    
    private func getBestImageForHeight(_ height: CGFloat, index: Int) -> Dictionary<String, Any>? {
        if let photo = photos?[index] {
            if let sizes = photo["sizes"] as? Array<Dictionary<String, Any>> {
                var retVal = sizes.first
                for size in sizes {
                    if (size["height"] as! Int) > (retVal!["height"] as! Int) && CGFloat(size["height"] as! Int) < height {
                        retVal = size
                    }
                }
                return retVal
            }
        }
        return nil
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        collectionView?.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
        
        collectionView?.register(UINib(nibName: "PhotoCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: PhotoCollectionViewCell.identifier)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.identifier, for: indexPath) as! PhotoCollectionViewCell
        
        if let bestImage = getBestImageForHeight(cell.bounds.height * UIScreen.main.scale, index: indexPath.item) {
            cell.imageUrl = URL(string: bestImage["src"] as! String)
        }
        cell.title = photos?[indexPath.item]["text"] as? String
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showSinglePhoto", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSinglePhoto", let destinationVc = segue.destination as? SinglePhotoViewController {
            if let selectedItemIndex = collectionView?.indexPathsForSelectedItems?.first?.item {
                if let bestImage = getBestImageForHeight(destinationVc.view.bounds.height * UIScreen.main.scale, index: selectedItemIndex) {
                    destinationVc.photoUrl = URL(string: bestImage["src"] as! String)
                }
            }
        }
    }
}
